#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <string.h>

#ifndef CHAR_SIZE
#define CHAR_SIZE 8
#endif

#ifndef EOF
#define EOF -1
#endif


char* file_get_str(char* fpath, char* buf);


