#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <string.h>

#include <ncurses.h>

#define TOILET "/usr/bin/toilet"
#define NANO "/usr/bin/nano"
#define STDOUT 1

char* title(char* text)
{

  char* titleText = (char*) malloc(2000);
  
  int pipefd[2];
  pipe(pipefd);
  
  pid_t forkStatus = fork();
  if (forkStatus == 0)
    {
      dup2(pipefd[1], STDOUT);
      
      char* args[] = {TOILET, text};
      execv(TOILET, args);
    }
  else
    {
      wait(NULL);
      read(pipefd[0], titleText, 2000);
    }
  return titleText;
}

char* identify(char* s)
{
  char msg[] = "IDENTIFY THYSELF\n";
  printw("%s", msg);
  getstr(s);
  return s;
}

void use_nano()
{
  int pipefd[2];
  pipe(pipefd);
  //def_prog_mode();
  
  pid_t forkStatus = fork();
  if (forkStatus == 0)
    {
      char* args[] = {NANO, NULL};
      execv(NANO, args);
    }
  else
    {
      def_prog_mode();
      wait(NULL);
      reset_prog_mode();
      refresh();
    }
}

int center_x(char* str, WINDOW* w)
{
  int row, col;
  getmaxyx(w, row, col);

  int L = strlen(str);

  return (col - L) / 2;
}

void greet(char* str, WINDOW* w)
{
  int row, col;
  getmaxyx(w, row, col);
  char* s = (char*) malloc( strlen(str) );
  strcpy(s, str);
  char* msg = "Hello, ";
  char* buf = (char*) malloc ( strlen(msg) );
  strcpy(buf, msg);
  strcat(buf, s);

  int y1 = row - 4;
  int x1 = center_x(buf, w);
  mvprintw(y1, x1, "%s\n", buf);


  char* welcome = "Welcome to the SODA SYSTEM\n";
  int y2 = row - 3;
  int x2 = center_x(welcome, w);
  
  mvprintw(y2, x2, "%s", welcome);
  free(buf);
  free(s);
}

void homescr()
{
  char* t = title("Soda System");
  printw("%s", t);
  refresh();
  free(t); 
}


int main()
{
  char s[80];
  char* t = title("Soda System");

  initscr();

  printw("%s", t);
  refresh();
  free(t);
  
  cbreak();
  
  identify(s);
  greet(s, stdscr);
  refresh();

  char ch;
  ch = getch();
  if (ch == 'n')
    {
      def_prog_mode();
      endwin();
      system(NANO);
      reset_prog_mode();
      refresh();
      homescr();
    }
  else
    {
      homescr(); 
    }
  refresh();
  getch();
  endwin();
  
  return 0;
}
