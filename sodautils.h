#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <string.h>

#define WHICH "/bin/which"

#ifndef MAX_PATH
#define MAX_PATH 4096
#endif

#define CHAR_SIZE 8

char* get_binpath(char* name, char* s);
