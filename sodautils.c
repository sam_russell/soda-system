#include "sodautils.h"

char* get_binpath(char* name, char* s)
{
  int pipefd[2];
  pipe(pipefd);
  //char* buf = (char*) malloc(MAX_PATH);
  char buf[MAX_PATH];
  s = (char*) NULL;
  s = (char*) malloc(MAX_PATH);

  pid_t forkStatus = fork();
  if (forkStatus == 0)
    {
      dup2(pipefd[1], 1);
      char* args[] = {WHICH, name, NULL};
      execv(WHICH, args);
      close(pipefd[1]);
    }
  else
    {
      wait(NULL);
      int chars_read = read(pipefd[0], buf, MAX_PATH);
      if (chars_read  == -1)
	{
	  printf("Error on reading from which\n");
	}
      close(pipefd[0]);
      for (int i = 0; i < (chars_read - 1); i++)
	{
	  s[i] = buf[i];
	}
      s[chars_read] = '\0';
    }
  return s;
}

