#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include "sodagraphics.h"
#include "sodaio.h"
#include "sodautils.h"

#include <ncurses.h>
#include <menu.h>

void homescr(char* t);
char* identify(char* n);
void greet(char* n, WINDOW* w);
char* input_prompt(char* p_msg, char* o, WINDOW* w);
char* menu(WINDOW* w, asciiart banner, char* choices[], int n_choices);
void launch_subproc(char* sp, char* sp_args[]);

