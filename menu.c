#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <string.h>

#include <ncurses.h>
#include <menu.h>

#define TOILET "/usr/bin/toilet"
#define NANO "/usr/bin/nano"
#define STDOUT 1
#define ARRAY_SIZE(a) (sizeof(a) / sizeof(a[0]))

int center_x(char* str, WINDOW* w)
{
  int row, col;
  getmaxyx(w, row, col);

  int L = strlen(str);

  return (col - L) / 2;
}

char* title(char* text)
{

  char* titleText = (char*) malloc(2000);
  
  int pipefd[2];
  pipe(pipefd);
  
  pid_t forkStatus = fork();
  if (forkStatus == 0)
    {
      dup2(pipefd[1], STDOUT);
      
      char* args[] = {TOILET, text};
      execv(TOILET, args);
    }
  else
    {
      wait(NULL);
      read(pipefd[0], titleText, 2000);
    }
  return titleText;
}


void homescr()
{
  char* t = title("Soda System");
  printw("%s", t);
  refresh();
  free(t); 
}

char* choices[] = { "nano",
		    "cowsay",
		    "quit", };


int main()
{

  ITEM** some_items;
  int key;
  MENU *some_menu;
  ITEM* cur_item;
  WINDOW* menu_winda;
  
  initscr();

  cbreak();
  noecho();
  keypad(stdscr, TRUE);

  int n_choices = ARRAY_SIZE(choices);
  some_items = (ITEM**) calloc( n_choices + 1, sizeof(ITEM*) );

  for (int i = 0; i < n_choices; i++)
    {
      some_items[i] = new_item(choices[i], choices[i]);
    }
  some_items[n_choices] = (ITEM*) NULL;
  

  some_menu = new_menu( (ITEM**) some_items );

  menu_winda = newwin(10, 40, 20, 4);
  keypad(menu_winda, TRUE);


  printw(" \n");
  homescr();
  refresh();
  set_menu_win(some_menu, menu_winda);
  set_menu_sub(some_menu, derwin(menu_winda, 6, 38, 1, 1) );
  box(menu_winda, 0, 0);
  refresh();
  
  post_menu(some_menu);
  wrefresh(menu_winda);

  key = getch();

  while ( (key = getch()) != 'q')
    {
      switch (key)
	{
	case KEY_DOWN:
	  menu_driver(some_menu, REQ_DOWN_ITEM);
	  wrefresh(menu_winda);
	  break;

	case KEY_UP:
	  menu_driver(some_menu, REQ_UP_ITEM);
	  wrefresh(menu_winda);
	  break;

	case 'p':
	  printw("hi\n");
	  refresh();

	case 10:
	  cur_item = current_item(some_menu);
	  char* n = (char*) item_name(cur_item);
	  printw("your choice: %s\n", n);
	  wrefresh(menu_winda);
	  break;
	  }
    }
  
  endwin();

  return 0;
  
}
