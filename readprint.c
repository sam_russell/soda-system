#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <string.h>

#include <ncurses.h>
#include <menu.h>

#define CHAR_SIZE 8

char* read_hdln(char* fpath)
{
  FILE* f = fopen(fpath, "r");
  char buf[2000];
  int c = fgetc(f);
  int pos = 0;
  buf[0] = c;
  while (c != -1)
    {
      c = fgetc(f);
      buf[++pos] = c;
    }
  printf("last char: %d", buf[pos]);
  char* s = (char*) malloc(strlen(buf) );
  for (int i = 0; i < strlen(buf); ++i)
    {
      s[i] = buf[i];
    }
  return s;
}

int main()
{
  char* s = read_hdln("/home/sam/soda-system/headline");
  printf("%s\n", s);
  
  return 0;
}
