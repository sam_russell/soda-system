
#include <string.h>

#include "sodasystem.h"


#ifndef ARRAY_SIZE
#define ARRAY_SIZE(a) (sizeof(a) / sizeof(a[0]))
#endif

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif


void homescr(char* t)
{
  printw("%s", t);
}

char* identify(char* n)
{
  char msg[] = "IDENTIFY THYSELF\n";
  printw("%s", msg);
  cbreak();
  getstr(n);
  return n;
}

char* input_prompt(char* p_msg, char* o, WINDOW* w)
{ 
  int row, col;
  int p_width, p_height;
  getmaxyx(w, row, col);
  p_width = width(p_msg);
  p_height = height(p_msg);

  int win_x, win_y;
  win_x = (col - p_width) / 2;
  win_y = (row - p_height) / 2;

  WINDOW* p_win;
  p_win = newwin(p_height + 3, p_width + 4, win_y, win_x);
  box(p_win, 0, 0);
  mvwprintw(p_win, 1, 1, p_msg);
  wrefresh(p_win);
  refresh();
  echo();
  cbreak();
  mvwgetstr(p_win, 2, 2, o);
  return o;
}

void greet(char* n, WINDOW* w)
{
  int row, col;
  getmaxyx(w, row, col);

  char greet_msg[] = "Hello, ";
  char* msg;
  strcat(greet_msg, n);
  msg = greet_msg;

  int y_msg = row - 4;
  int x_msg = (col - strlen(msg)) / 2;
  mvprintw(y_msg, x_msg, "%s\n", msg);
  
  char* welcome = "Welcome to the SODA SYSTEM\n";
  int y_welcome = row - 3;
  int x_welcome = (col - strlen(welcome)) / 2;
  mvprintw(y_welcome, x_welcome, "%s", welcome);
}

char* menu(WINDOW* w, asciiart banner, char* choices[], int n_choices)
{
  char* choice;
  ITEM** menu_items;
  int key_down;
  MENU* menu;
  ITEM* cur_item;
  WINDOW* menu_win;

  int row, col;
  getmaxyx(w, row, col);

  // int n_choices = ARRAY_SIZE(choices);
  // printw("%d\n", n_choices);
  menu_items = (ITEM**) calloc( n_choices + 1, sizeof(ITEM*) );
  for (int i = 0; i < n_choices; i++)
    {
      menu_items[i] = new_item(choices[i], "");
    }
  menu_items[n_choices] = (ITEM*) NULL;

  menu = new_menu( (ITEM**) menu_items);

  int y;
  y = banner.h - 2;
  int x;
  x = 40;

  int v;
  v = n_choices + 2;
  menu_win = newwin(10, 40, 10, 20);
  keypad(menu_win, TRUE);
  noecho();
  raw();
  set_menu_win(menu, menu_win);
  set_menu_sub(menu,
	       derwin(menu_win,
		      y - 4,
		      x - 2,
		      1,
		      1) );
  box(menu_win, 0, 0);
  post_menu(menu);
  wrefresh(menu_win);

  refresh();
  key_down = wgetch(menu_win);
  raw();

  while(key_down != 'q')
    {
      key_down = wgetch(menu_win);
      switch (key_down)
	{
	case KEY_DOWN:
	  menu_driver(menu, REQ_DOWN_ITEM);
	  wrefresh(menu_win);
	  break;
	case KEY_UP:
	  menu_driver(menu, REQ_UP_ITEM);
	  wrefresh(menu_win);
	  break;
	case 10:
	  cur_item = current_item(menu);
	  choice = (char*) item_name(cur_item);
	  return choice;
	  printw("i ditnt return\n");
	  break;
	}
      if (key_down == 10)
	{
	  return choice;
	}
    }
  return choice;
}

void launch_subproc(char* sp, char* sp_args[])
{
  int pipefd[2];
  pipe(pipefd);

  char* bin_path;
  bin_path = get_binpath(sp, bin_path);

  int args_count;
  args_count = ARRAY_SIZE(sp_args);
  int total_args;
  total_args = args_count + 2;
  int last_arg;
  last_arg = total_args - 1;

  pid_t fork_status = fork();
  if (fork_status == 0)
    {
      char* args[total_args];
      args[0] = bin_path;
      for (int i = 0; i < args_count; ++i)
	{
	  args[i + 1] = sp_args[i];
	}
      args[last_arg] = NULL;

      execv(bin_path, args);
    }
  else
    {
      def_prog_mode();
      endwin();
      wait(NULL);
      reset_prog_mode();
      raw();
      int ch = 0;
      while (ch != 10)
	{
	  ch = getch();
	}
    }
}
