#include "sodasystem.h"

#ifndef ARRAY_SIZE
#define ARRAY_SIZE(a) (sizeof(a) / sizeof(a[0]))
#endif

void run_cowsay(char* selection, WINDOW* w)
{
  char prompt_msg[] = "What do you want the cow to say?\n";
  char* prompt_o = (char*) malloc(2000);
  prompt_o = input_prompt(prompt_msg, prompt_o, w);
  refresh();
  char* cow_args[1] = { prompt_o };
  launch_subproc(selection, cow_args);
  free(prompt_o);
}

void run_nano(char* selection)
{
  char* null_char;
  null_char = NULL;
  char* nano_args[1] = { null_char };
  //def_prog_mode();
  //endwin();
  launch_subproc(selection, nano_args);
  //reset_prog_mode();
}

int menu_select(WINDOW* w, asciiart art_data)
{
  char* menu_choices[] = {"nano", "cowsay", "quit"};
  int n_choices;
  n_choices = ARRAY_SIZE(menu_choices);
  char* selection;
  selection = menu(w, art_data, menu_choices, n_choices);

  if (selection == "cowsay")
    {
      run_cowsay(selection, w);
      return 1;
    }
  else if (selection == "nano")
    {
      run_nano(selection);
      return 1;
    }
  else if (selection == "quit")
    {
      return 0;
    }
}

int main(int argc, char* argv[])
{
  char* art = (char*) malloc(2000);
  art = file_get_str("/home/sam/soda-system/headline", art);
  asciiart art_data = { height(art),
			width(art),
			art };
  initscr();
  cbreak();
  homescr(art);
  refresh();


  char* username = (char*) malloc(80);
  username = identify(username);
  refresh();

  greet(username, stdscr);
  refresh();

  int status;
  status = menu_select(stdscr, art_data);;
  while(status != 0)
    {
      status = menu_select(stdscr, art_data);
      erase();
      homescr(art);
      refresh();
    }
  
  
  refresh();

  char ch;
  ch = getch();
  printw("ok bye now\n");
  endwin();

  
  free(art);
  free(username);

  return 0;
}

