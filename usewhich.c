#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <string.h>

#define WHICH "/bin/which"
#define CHAR_SIZE 8

char* get_binpath(char* name)
{
  int pipefd[2];
  pipe(pipefd);
  char* buf = (char*) malloc(4096);

  pid_t forkStatus = fork();
  if (forkStatus == 0)
    {
      dup2(pipefd[1], 1);
      char* args[] = {WHICH, name};
      execv(WHICH, args);
    }
  else
    {
      wait(NULL);
      read(pipefd[0], buf, 4096);
      close(pipefd[0]);
    }
  return buf;
}

int main(int argc, char* argv[])
{
  printf("%s\n", get_binpath(argv[1]));
}
