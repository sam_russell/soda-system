#include "graphics.h"

int height(char* s)
{
  int L;
  L = strlen(s);
  int lines_count;
  lines_count = 1;
  char c;
  for (int i = 0; i < L; ++i)
    {
      c = s[i];
      if (c == LINE_BREAK)
	{
	  ++lines_count;
	}
    }
  return lines_count;
}

int width(char* s)
{
  int L;
  L = strlen(s);
  int longest;
  int longest = 1;
  int len_buffer = 0;

  for (int i = 0; i < L; ++i)
    {
      if (s[i] != LINE_BREAK)
	{
	  ++len_buffer;
	}
      else
	{
	  if (len_buffer > longest)
	    {
	      longest = len_buffer;
	    }
	  len_buffer = 0;
	}
    }
  return longest;
}
