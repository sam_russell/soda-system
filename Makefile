CC=gcc
CFLAGS=-I.
LDLIBS=-lncurses -lmenu
DEPS = sodautils.h sodaio.h sodagraphics.h sodasystem.h

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

test: test.o sodasystem.o sodautils.o sodaio.o sodagraphics.o
	$(CC) -o test test.o sodasystem.o \
	sodautils.o sodaio.o sodagraphics.o $(CFLAGS) $(LDLIBS)

clean:
	rm *.o
