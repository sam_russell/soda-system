#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef CHAR_SIZE
#define CHAR_SIZE 8
#endif

#ifndef EOF
#define EOF -1
#endif

#ifndef LINE_BREAK
#define LINE_BREAK '\n'
#endif

typedef struct
{
  int h;
  int w;
  char* string;
} asciiart;

int height(char* s);
int width(char* s);
